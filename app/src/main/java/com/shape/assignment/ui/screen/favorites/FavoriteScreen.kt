package com.shape.assignment.ui.screen.favorites

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.shape.assignment.data.model.BreedEntity
import com.shape.assignment.data.model.ImageEntity
import com.shape.assignment.ui.screen.GeneralScaffold
import com.shape.assignment.ui.screen.images.LoadImage
import timber.log.Timber

@Composable
fun FavoriteScreen(viewModel: FavoriteViewModel, navController: NavHostController){

    val images by viewModel.imageList.collectAsState(initial = listOf())
    val breeds by viewModel.breedList.collectAsState()

    GeneralScaffold(title = "favorites".uppercase(), navController = navController) {
        FavoriteContent(images,breeds){
            Timber.d("filter is $it")
            viewModel.filrer.tryEmit(it)
        }
    }


}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun FavoriteContent(images: List<ImageEntity>, breeds: List<BreedEntity>,
                    changeFilter:(String)->Unit) {

    Column {

        DropDown(breeds, changeFilter)




        LazyVerticalGrid(
            cells = GridCells.Adaptive(minSize = 150.dp)
        ) {

            items(images){imageEntity->
                ImageItem(imageEntity)
            }

        }

    }

}

@Composable
private fun DropDown(
    breeds: List<BreedEntity>,
    changeFilter: (String) -> Unit
) {
    Column(Modifier.padding(10.dp)) {

        var expanded by remember { mutableStateOf(false) }

        var text by rememberSaveable {
            mutableStateOf("No Filter")
        }

        Button(onClick = { expanded = !expanded }) {
            Text(text)
            Icon(
                imageVector = Icons.Filled.ArrowDropDown,
                contentDescription = null,
            )
        }
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
        ) {

            DropdownMenuItem(onClick = {
                expanded = false
                text= "No Filter"
                changeFilter("")
            }) {
                Text(text = "No Filter")
            }

            breeds.forEach { breedEntity ->
                DropdownMenuItem(onClick = {
                    expanded = false
                    text= breedEntity.breed
                    changeFilter(breedEntity.breed)
                }) {
                    Text(text = breedEntity.breed)
                }
            }
        }

    }
}

@Composable
private fun ImageItem(imageEntity: ImageEntity) {

     LoadImage(imageEntity)

}

