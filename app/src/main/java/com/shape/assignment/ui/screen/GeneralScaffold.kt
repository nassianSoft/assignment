package com.shape.assignment.ui.screen

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.navigation.NavController

@Composable
fun GeneralScaffold(title:String,navController:NavController,actions:@Composable ()->Unit={}
                    ,content:@Composable ()->Unit){
    val scaffoldState = rememberScaffoldState()
    Scaffold (scaffoldState=scaffoldState
        ,topBar = {
            TopAppBar(title = { Text(text = title) },
                navigationIcon = {
                    IconButton(onClick = { navController.navigateUp()}) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = "Back",
                        )
                    }
                },actions = {
                    actions()
                })
        },content = {
            content() }
    )
}