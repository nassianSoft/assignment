package com.shape.assignment.ui.screen.images

import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.transform.CircleCropTransformation
import com.shape.assignment.R
import com.shape.assignment.data.model.ImageEntity
import com.shape.assignment.ui.screen.GeneralScaffold
import com.shape.assignment.util.showMessage
import kotlinx.coroutines.flow.consumeAsFlow
import timber.log.Timber

@Composable
fun ImageScreen(viewModel: ImagesViewModel, navController: NavHostController, breed: String){

    val images by viewModel.imageList.collectAsState()

    LaunchedEffect(key1 = Unit){
        viewModel.loadImages(breed)
    }

    val context = LocalContext.current
    LaunchedEffect(key1 = Unit){
        viewModel.messageChannel.consumeAsFlow().collect{
            showMessage(context,it)
        }
    }

    GeneralScaffold(title = breed.uppercase(), navController = navController) {
        ImageContent(images){
            Timber.i("change favorite $it")
            viewModel.changeFavorite(it)
        }
    }

}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ImageContent(images: List<ImageEntity>, changeFavorite: (ImageEntity)->Unit ) {



    LazyVerticalGrid(
        cells = GridCells.Adaptive(minSize = 150.dp)
    ) {

        items(images){imageEntity->
            ImageItem(imageEntity, changeFavorite)
        }
    }


}

@Composable
private fun ImageItem(imageEntity: ImageEntity, changeFavorite: (ImageEntity) -> Unit) {


        Surface(color = MaterialTheme.colors.background) {
            LoadImage(imageEntity)

            Heart(imageEntity, changeFavorite)

        }


}

@Composable
private fun Heart(
    imageEntity: ImageEntity,
    changeFavorite: (ImageEntity) -> Unit
) {
    val color = colorState(favorite = imageEntity.favorite)

    Image(
        painter = painterResource(R.drawable.cards_heart),
        contentDescription = null,
        colorFilter = ColorFilter.tint(color.value),
        modifier = Modifier
            .padding(10.dp)
            .size(32.dp)
            .clickable {
                changeFavorite(imageEntity)
            }
    )
}

@Composable
fun LoadImage(imageEntity: ImageEntity) {
    val painter =
        rememberAsyncImagePainter(
            ImageRequest.Builder(LocalContext.current)
                .data(data = imageEntity.url)
                .apply(block = fun ImageRequest.Builder.() {
                    transformations(
                        CircleCropTransformation()
                    )
                }).build()
        )
    Image(
        painter = painter,
        contentDescription = null,
        modifier = Modifier
            .padding(10.dp)
            .aspectRatio(1f),
        contentScale = ContentScale.Crop,
    )
}

@Composable
fun colorState(favorite: Boolean): State<Color> {
    return animateColorAsState(
        if (favorite) Color.Red else Color.White
    )
}
