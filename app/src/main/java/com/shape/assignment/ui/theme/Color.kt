package com.shape.assignment.ui.theme


import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Red700 = Color(0xffdd0d3c)
val Red800 = Color(0xffd00036)
val Red900 = Color(0xffc20029)


val Red200 = Color(0xfff297a2)
val Red300 = Color(0xffea6d7e)

val Green = Color(0xff00c853)
val DarkGreen= Color(0xff009624)
val LightGreen= Color(0xff5efc82)


val DarkYellow= Color(0xfffdd835)

val DribbleRed= Color(0xffff4144)
val DribbleRedNight= Color(0xC3FF4144)
val DribbleGreen= Color(0xFF47b2a5)
val DribbleBlack= Color(0xff24201b)
val DribbleBack= Color(0xffd1cec4)
val AndroidStudioBack= Color(0xFF3B3F41)