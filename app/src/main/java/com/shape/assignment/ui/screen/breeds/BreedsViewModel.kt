package com.shape.assignment.ui.screen.breeds

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shape.assignment.data.DogRepository
import com.shape.assignment.data.model.BreedEntity
import com.shape.assignment.data.remote.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class BreedsViewModel @Inject constructor(dogRepository: DogRepository):ViewModel() {

    private val _breedList = MutableStateFlow(listOf<BreedEntity>())
    val breedList = _breedList.asStateFlow()

    private val _messageChannel = Channel<String>()
    val messageChannel : ReceiveChannel<String> = _messageChannel

    init {
        viewModelScope.launch {
            dogRepository.getAllBreeds()
                .onStart {

                    launch(Dispatchers.IO) {
                        fetchBreedsFromApi(dogRepository)
                    }

                }.flowOn(Dispatchers.IO)
                .collect{
                    _breedList.value = it
                }
        }

    }

    private suspend fun fetchBreedsFromApi(dogRepository: DogRepository) {
        when (val result = dogRepository.getOnlineBreeds()) {
            is Result.Success -> {
                Timber.d(" success ${result.value.message}")
                dogRepository.insertBreeds(result.value.message.map { BreedEntity(it) })
            }
            is Result.Failure -> {
                Timber.d(" failure ${result.msg}")
                withContext(Dispatchers.Main) {
                    Timber.i("sending msg : ${result.msg}")
                    _messageChannel.send(result.msg)
                }
            }
        }
    }

}