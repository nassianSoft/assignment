package com.shape.assignment.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val lightColors= lightColors(
    primary = Red700,
    primaryVariant = DribbleRed,
    onPrimary = Color.White,
    secondary = Green,
    secondaryVariant = DribbleRed,
    onSecondary = Color.White,
    onError = Red800,
    background = DribbleBack
)

private val darkColors = darkColors(
    primary = DribbleRedNight,
    primaryVariant = Red700,
    onPrimary = Color.Black,
    secondary = Green,
    secondaryVariant =DribbleGreen ,
    onSecondary = Color.Black,
    error = Red200,
    background = AndroidStudioBack

)

@Composable
fun AssignmentTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        darkColors
    } else {
        lightColors
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}