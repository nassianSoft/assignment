package com.shape.assignment.ui.screen.images

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shape.assignment.data.DogRepository
import com.shape.assignment.data.model.ImageEntity
import com.shape.assignment.data.remote.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ImagesViewModel @Inject constructor(private val dogRepository: DogRepository):ViewModel() {

    private val _imageList = MutableStateFlow(listOf<ImageEntity>())
    val imageList = _imageList.asStateFlow()

    private val _messageChannel = Channel<String>()
    val messageChannel : ReceiveChannel<String> = _messageChannel


    fun loadImages(breed:String){
        viewModelScope.launch {
            dogRepository.getImages(breed)
                .onStart {
                    launch(Dispatchers.IO) {

                        fetchOnlineImages(breed)
                    }
                }.flowOn(Dispatchers.IO)
                .collect{
                    _imageList.value = it
                }
        }
    }

    fun changeFavorite(imageEntity: ImageEntity){
        viewModelScope.launch(Dispatchers.IO) {
            dogRepository.updateImage(imageEntity.copy(favorite = !imageEntity.favorite))
        }
    }

    private suspend fun fetchOnlineImages(breed: String) {
        when (val result = dogRepository.getOnlineImages(breed)) {
            is Result.Success -> {
                dogRepository.insertImages(
                    result.value.message.map {
                        ImageEntity(url = it, breed = breed, favorite = false)
                    }
                )
            }
            is Result.Failure -> {
                withContext(Dispatchers.Main) {
                    Timber.i("sending msg : ${result.msg}")
                    _messageChannel.send(result.msg)
                }
            }
        }
    }


}