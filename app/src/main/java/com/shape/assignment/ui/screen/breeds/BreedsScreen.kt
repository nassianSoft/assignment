package com.shape.assignment.ui.screen.breeds

import android.content.res.Configuration
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.R
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.shape.assignment.data.model.BreedEntity
import com.shape.assignment.ui.theme.AssignmentTheme
import com.shape.assignment.util.showMessage
import kotlinx.coroutines.flow.consumeAsFlow
import timber.log.Timber

@Composable
fun BreedsScreen(viewModel: BreedsViewModel, navController: NavHostController) {

    val breeds by viewModel.breedList.collectAsState()

    val context = LocalContext.current
    LaunchedEffect(key1 = Unit){
        viewModel.messageChannel.consumeAsFlow().collect{
            showMessage(context,it)
        }
    }



    BreedsContent(breeds,navigateToFavorites = {navController.navigate("favorites")})
    { breedEntity: BreedEntity ->
        Timber.i("navigate to ${breedEntity.breed}")
        navController.navigate("images/${breedEntity.breed}")
    }

}

@OptIn(ExperimentalFoundationApi::class, androidx.compose.material.ExperimentalMaterialApi::class)
@Composable
fun BreedsContent(breeds: List<BreedEntity>, navigateToFavorites: () -> Unit
                  , navigateToImages: (breed:BreedEntity) -> Unit) {


    Column {


        val configuration = LocalConfiguration.current

        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            Image(
                painter = painterResource(id = com.shape.assignment.R.drawable.ic_dog_banner),
                contentDescription = null,
                contentScale = ContentScale.FillWidth,
                modifier = Modifier
                    .fillMaxWidth()
            )
        }
        
        Surface(modifier = Modifier
            .padding(10.dp)
            .fillMaxWidth(1f),
            shape = RoundedCornerShape(50.dp, 50.dp, 50.dp, 50.dp),
            color = MaterialTheme.colors.secondaryVariant,
            onClick = { navigateToFavorites() } ) {
            Text(text = "Favorites",Modifier.padding(10.dp),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.h5
            )
        }

        LazyColumn(contentPadding = PaddingValues(bottom = 100.dp) ){
            items(breeds){breedEntity->
                BreedsItem(breedEntity, navigateToImages )
            }
        }
    }


}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BreedsItem(breedEntity: BreedEntity, navigateToImages: (breed: BreedEntity) -> Unit) {
    Surface(
        onClick = { navigateToImages(breedEntity) },
        Modifier
            .padding(10.dp)
            .fillMaxWidth(1f),
        shape = RoundedCornerShape(5.dp, 50.dp, 5.dp, 50.dp),
        color = MaterialTheme.colors.secondaryVariant
    ) {

        Image(
            painter = painterResource(id = com.shape.assignment.R.drawable.ic_dog_fade),
            contentDescription = null,
            contentScale = ContentScale.Fit,
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp)
        )

        Text(
            modifier = Modifier.padding(32.dp),
            text = breedEntity.breed,
            color = Color.White,
            textAlign = TextAlign.Center,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = TextStyle(fontSize = 34.sp,
                shadow = Shadow(
                    color = Color.Gray,
                    offset = Offset(5f, 5f),
                    blurRadius = 5f
                )
            ),
        )
    }
}




@Composable
@Preview
fun BreedEntityPreview(){
    AssignmentTheme {
        val breeds = listOf(BreedEntity("affenpinscher11111111111111"),
            BreedEntity("african"),
            BreedEntity("airedale"))

        BreedsContent(breeds = breeds, navigateToImages = {}, navigateToFavorites = {})
    }
}


