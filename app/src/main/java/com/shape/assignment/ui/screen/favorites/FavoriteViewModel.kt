package com.shape.assignment.ui.screen.favorites

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shape.assignment.data.DogRepository
import com.shape.assignment.data.model.BreedEntity
import com.shape.assignment.data.model.ImageEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(dogRepository: DogRepository):ViewModel() {

    val filrer = MutableStateFlow("")

    private val _breedList = MutableStateFlow(listOf<BreedEntity>())
    val breedList = _breedList.asStateFlow()

    private val _imageList = MutableStateFlow(listOf<ImageEntity>())
    val imageList = _imageList.combine(filrer){list,fil->

        if (fil.isEmpty()) list else list.filter { it.breed==fil }
    }


    init {
        viewModelScope.launch {

            launch {
                dogRepository.getAllBreeds()
                    .flowOn(Dispatchers.IO)
                    .collect{
                        _breedList.value = it
                    }
            }

            launch {
                dogRepository.getFavorites()
                    .flowOn(Dispatchers.IO)
                    .collect{
                        _imageList.value=it
                    }
            }

        }
    }

}