package com.shape.assignment.di

import android.content.Context
import androidx.room.Room
import com.shape.assignment.data.local.DogDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context):DogDatabase=Room
        .databaseBuilder(context,DogDatabase::class.java,DogDatabase.databaseName)
        .build()
}