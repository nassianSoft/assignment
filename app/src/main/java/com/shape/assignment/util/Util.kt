package com.shape.assignment.util

import android.content.Context
import android.widget.Toast


fun showMessage(context: Context, msg:String){
        Toast.makeText(context.applicationContext,msg, Toast.LENGTH_LONG).show()
}