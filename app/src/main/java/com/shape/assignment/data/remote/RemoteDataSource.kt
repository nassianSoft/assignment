package com.shape.assignment.data.remote

import com.shape.assignment.data.model.BreedsResponse
import com.shape.assignment.data.model.ImageResponse
import com.shape.assignment.data.model.NetworkResponse
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val dogApi: DogApi) {

    suspend fun getBreeds(): Result<BreedsResponse>{
        return processNetworkResponse {
            dogApi.getBreeds()
        }
    }

    suspend fun getImages(breed:String): Result<ImageResponse>{
        return processNetworkResponse {
            dogApi.getImage(breed)
        }
    }


    private suspend fun <T:NetworkResponse> processNetworkResponse(
        action: suspend () -> Response<T>
    ): Result<T> {

        return try {

            val response = action()

            return if (response.isSuccessful) {
                val result = response.body()
                if (result != null) {
                    Result.Success(result)
                } else {
                    Result.Failure("No Result")
                }
            } else {
                Result.Failure( "Something went wrong")
            }

        } catch (e: Exception) {
            println(e)
            Result.Failure("Unable to connect to the server")
        }
    }

}

sealed class Result<T> {
    class Failure<T>(val msg: String) : Result<T>()
    class Success<T>(val value: T) : Result<T>()
}