package com.shape.assignment.data

import com.shape.assignment.data.local.LocalDataSource
import com.shape.assignment.data.model.BreedEntity
import com.shape.assignment.data.model.BreedsResponse
import com.shape.assignment.data.model.ImageEntity
import com.shape.assignment.data.model.ImageResponse
import com.shape.assignment.data.remote.RemoteDataSource
import com.shape.assignment.data.remote.Result
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DogRepository @Inject constructor(private val remoteDataSource: RemoteDataSource
                                        ,private val localDataSource: LocalDataSource) {

    suspend fun getOnlineBreeds(): Result<BreedsResponse> = remoteDataSource.getBreeds()

    suspend fun getOnlineImages(breed:String): Result<ImageResponse> =
        remoteDataSource.getImages(breed)

    fun insertBreeds(breeds:List<BreedEntity>) = localDataSource.insertBreeds(breeds)

    fun insertImages(images:List<ImageEntity>) = localDataSource.insertImages(images)

    fun updateImage(image: ImageEntity) = localDataSource.updateImage(image)

    fun getAllBreeds(): Flow<List<BreedEntity>> = localDataSource.getAllBreeds()

    fun getImages(breed:String): Flow<List<ImageEntity>> = localDataSource.getImages(breed)

    fun getFavorites():Flow<List<ImageEntity>> = localDataSource.getFavorites()


}