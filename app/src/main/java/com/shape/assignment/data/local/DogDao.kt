package com.shape.assignment.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import androidx.room.Update
import com.shape.assignment.data.model.BreedEntity
import com.shape.assignment.data.model.ImageEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface DogDao {

    @Insert(onConflict = IGNORE)
    fun insertBreeds(breeds:List<BreedEntity>)

    @Insert(onConflict  = IGNORE)
    fun insertImages(images:List<ImageEntity>)

    @Update
    fun updateImage(image:ImageEntity)

    @Query("select * from BreedEntity")
    fun getAllBreeds():Flow<List<BreedEntity>>

    @Query("select * from ImageEntity where breed like :breed")
    fun getImages(breed:String):Flow<List<ImageEntity>>

    @Query("select * from ImageEntity where favorite=1 ")
    fun getFavorites():Flow<List<ImageEntity>>

}