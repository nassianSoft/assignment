package com.shape.assignment.data.model

data class BreedsResponse(override val message: List<String>, override val status: String) :
    NetworkResponse