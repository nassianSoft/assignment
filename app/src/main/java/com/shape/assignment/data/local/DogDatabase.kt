package com.shape.assignment.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.shape.assignment.data.model.BreedEntity
import com.shape.assignment.data.model.ImageEntity

@Database(entities = [BreedEntity::class,ImageEntity::class],version = 1,exportSchema = false)
abstract class DogDatabase: RoomDatabase() {

    abstract fun dogDao(): DogDao

    companion object{
        const val databaseName="dogDatabase"
    }
}