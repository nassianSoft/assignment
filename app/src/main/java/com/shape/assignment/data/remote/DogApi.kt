package com.shape.assignment.data.remote

import com.shape.assignment.data.model.BreedsResponse
import com.shape.assignment.data.model.ImageResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface DogApi {

    companion object{
        const val baseUrl = "https://dog.ceo/api/"
    }

    @GET("breeds/list")
    suspend fun getBreeds(): Response<BreedsResponse>

    @GET("breed/{breed}/images")
    suspend fun getImage( @Path("breed") breed:String): Response<ImageResponse>
}