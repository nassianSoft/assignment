package com.shape.assignment.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ImageEntity (
    @PrimaryKey
    val url:String,
    val breed:String,
    val favorite:Boolean
        )