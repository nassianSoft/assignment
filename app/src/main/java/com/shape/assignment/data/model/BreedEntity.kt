package com.shape.assignment.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class BreedEntity (
    @PrimaryKey
    val breed:String
        )