package com.shape.assignment.data.model

interface NetworkResponse {
    val message:List<String>
    val status: String
}