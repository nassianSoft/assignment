package com.shape.assignment.data.local

import com.shape.assignment.data.model.BreedEntity
import com.shape.assignment.data.model.ImageEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LocalDataSource @Inject constructor(dogDatabase: DogDatabase) {

    private val dogDao=dogDatabase.dogDao()


    fun insertBreeds(breeds:List<BreedEntity>) = dogDao.insertBreeds(breeds)

    fun insertImages(images:List<ImageEntity>) = dogDao.insertImages(images)

    fun updateImage(image: ImageEntity) = dogDao.updateImage(image)

    fun getAllBreeds(): Flow<List<BreedEntity>> = dogDao.getAllBreeds()

    fun getImages(breed:String): Flow<List<ImageEntity>> = dogDao.getImages(breed)

    fun getFavorites():Flow<List<ImageEntity>> = dogDao.getFavorites()


}