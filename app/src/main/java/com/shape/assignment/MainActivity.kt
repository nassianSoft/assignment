package com.shape.assignment

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.shape.assignment.ui.screen.breeds.BreedsScreen
import com.shape.assignment.ui.screen.breeds.BreedsViewModel
import com.shape.assignment.ui.screen.favorites.FavoriteScreen
import com.shape.assignment.ui.screen.favorites.FavoriteViewModel
import com.shape.assignment.ui.screen.images.ImageScreen
import com.shape.assignment.ui.screen.images.ImagesViewModel
import com.shape.assignment.ui.theme.AssignmentTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            AssignmentTheme {
                val navController = rememberNavController()
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    AppCompose(navController)
                }
            }
        }
    }
}

@Composable
fun AppCompose(navController: NavHostController) {

    NavHost(navController = navController, startDestination = "breeds"){

        composable("breeds"){
            val viewModel = hiltViewModel<BreedsViewModel>()
            BreedsScreen(viewModel = viewModel, navController= navController )
        }

        composable("images/{breed}"){
            val viewModel = hiltViewModel<ImagesViewModel>()
            val breed = it.arguments?.getString("breed")
            breed?:return@composable
            ImageScreen(viewModel = viewModel, navController = navController, breed)
        }

        composable("favorites"){
            val viewModel = hiltViewModel<FavoriteViewModel>()
            FavoriteScreen(viewModel = viewModel, navController = navController)
        }

    }
}




@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    AssignmentTheme {
        Greeting("Android")
    }
}